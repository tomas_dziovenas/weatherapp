<?php
class AccuWeather implements WeatherInterface
{
    private $city_name;
    private $api_key;
    
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }
    
    public function getTemperature($city)
    {
        $city = str_replace(' ', '', $city);
            
        //get city code from city name
        $url = "http://dataservice.accuweather.com/locations/v1/search?apikey=".$this->api_key."&q=".$city;
        $response = \Httpful\Request::get($url)->expectsJson()->send();
               
        //if no cities found
        if (count($response->body) == 0) {
            throw new CityNotFoundException('no such city');
        }
        
        //if bad API key given
        if (isset($response->body->Code)) {
            if ($response->body->Code == "Unauthorized") {
                throw new BadKeyException('bad api key');
            }
        }
        
        //get city key from request
        $city_key = $response->body[0]->Key;
        
        //get city name, because other request doesn't give it
        $this->city_name = $response->body[0]->EnglishName;
        
        //get city temperature
        echo $this->formJSON($this->getTemperatureFromCityCode($city_key));
    }
        
    public function getTemperatureFromCityCode($city_code)
    {
        //get city temperature details
        $url = "http://dataservice.accuweather.com/currentconditions/v1/".$city_code."?apikey=".$this->api_key."&details=true";
        $response = \Httpful\Request::get($url)->expectsJson()->send();
            
        return $response;		
    }
    
    public function formJSON($response)
    {
        $formed_json = array();
        $formed_json["status"] = "success";
        $formed_json["city"] = $this->city_name;
        $formed_json["current_temp"] = $response->body[0]->Temperature->Metric->Value;
        $formed_json["min_temp"] = $response->body[0]->TemperatureSummary->Past6HourRange->Minimum->Metric->Value;
        $formed_json["max_temp"] = $response->body[0]->TemperatureSummary->Past6HourRange->Maximum->Metric->Value;
            
        //format from km/h to m/s
        $formed_json["wind_speed"] = number_format($response->body[0]->Wind->Speed->Metric->Value * 1000 / 3600, 1, '.', '');
            
        $formed_json["provider"] = "AccuWeather";
			
        return json_encode($formed_json);
    }
}
?>
