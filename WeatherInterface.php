<?php 
interface WeatherInterface
{
	public function getTemperature($city);
	public function formJSON($response);
}
?>