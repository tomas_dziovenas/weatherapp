<?php
class OpenWeatherMap implements WeatherInterface
{
    private $api_key;
    
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }
	
    public function getTemperature($city)
    {
        $city = str_replace(' ', '', $city);
        
        $url = "api.openweathermap.org/data/2.5/weather?q=".$city."&appid=".$this->api_key."&units=metric";
		$response = \Httpful\Request::get($url)->expectsJson()->send();
		
        //get response status code
        $status = $response->body->cod;
        switch ($status) {
            case "502":
                throw new CityNotFoundException('no such city');
                break;
            case "401":
                throw new BadKeyException('bad api key');
                break;
        }  
        //form json
        echo $this->formJSON($response);
    }
	
    public function formJSON($response)
    {
        $formed_json = array();	
        $formed_json["status"] = "success";
        $formed_json["city"] = $response->body->name;
        $formed_json["wind_speed"] = $response->body->wind->speed;
        $formed_json["current_temp"] = $response->body->main->temp;
        $formed_json["min_temp"] = $response->body->main->temp_min;
        $formed_json["max_temp"] = $response->body->main->temp_max;
        $formed_json["provider"] = "OpenWeatherMap";
        
        return json_encode($formed_json);
    }
}
?>