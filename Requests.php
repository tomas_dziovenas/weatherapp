<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once("WeatherInterface.php");
require('OpenWeatherMap.php');
require('AccuWeather.php');
require('Apixu.php');

$city = $_POST['city_name'];

class CityNotFoundException extends Exception{}
class BadKeyException extends Exception{}

$weather = array(
    new Apixu("6a7c449cdee74fd5888110558171401"), 
    new AccuWeather("0dkS9j54uOHDFBlQvY2ubwl8q8HcqjLq"), 
    new OpenWeatherMap("730c4ff0475823c43a7ef490bccc1435")
);

$counter = 0;

foreach ($weather as &$weather_object) {
    $counter++;
    try {
        $output = $weather_object->getTemperature($city);
        break;
    } catch (Exception $e) {
        if ($counter == count($weather)) {
            echo json_encode(array("error_message" => checkErrorType($e)[1], "status" => "error"));
        }
    }
}

function checkErrorType($e)
{
    if ($e instanceof BadKeyException) {
        return array(true, "Update API keys.");
    } elseif ($e instanceof CityNotFoundException) {
        return array(true, "No such city was found.");
    } else {
        return array(true, "An error occured.");
    }
}

?>
