<?php
class Apixu implements WeatherInterface
{
    private $api_key;
    
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }
    
    public function getTemperature($city)
    {
        $city = str_replace(' ', '', $city);
        
        //get temperature data from the city
		$url = "http://api.apixu.com/v1/forecast.json?key=".$this->api_key."&q=".$city."&days=1";
        
		$response = \Httpful\Request::get($url)->expectsJson()->send();
        
        //if response has error property, then throw errors
        //1003 - missing argument, 1006 - no city
        //2006 - invalid API key, 1005 - invalid API url
        if (isset($response->body->error)) {
            switch ($response->body->error->code) {
                case 1006:
				case 1003:
                    throw new CityNotFoundException('no such city');
                    break;
                case 2006:
                case 1005:
                    throw new BadKeyException('bad api key');
					break;
            }
        } else {
            //if no errors, form json
            echo $this->formJSON($response);
        }
    }
    
    public function formJSON($response)
    {
        $formed_json = array();
        $formed_json["status"] = "success";
        $formed_json["city"] = $response->body->location->name;
        $formed_json["wind_speed"] = number_format($response->body->current->wind_kph * 1000 / 3600, 1, '.', '');
        $formed_json["current_temp"] = $response->body->current->temp_c;
        $formed_json["min_temp"] = $response->body->forecast->forecastday[0]->day->mintemp_c;
        $formed_json["max_temp"] = $response->body->forecast->forecastday[0]->day->maxtemp_c;
        $formed_json["provider"] = "Apixu";
        
        return json_encode($formed_json);
    }
}
?>