<html>
    <head>
        <script
          src="https://code.jquery.com/jquery-3.1.1.min.js"
          integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
          crossorigin="anonymous"></script>
	
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	
	<body>
    
        <style>
		#theAlert{
			display:none;
		}
        </style>
        
        <div class="container" style="margin-top:20px">
        
            <div id="theAlert" class="alert alert-danger" role="alert">
              <strong>Warning!</strong> <span id="error_message"></span>
            </div>
	
            <div class="jumbotron">
                <h1>Weather Application</h1>
                <p>Type in city name and get details about weather.</p>
                
                <div class="row">
              
                    <div class="col-md-10 col-lg-8" style="padding:5px 10px">
                        <input name="search_field" type="text" class="form-control" placeholder="City Name" />
                    </div>
                    
                    <div class="col-md-2 col-lg-4" style="padding:5px 10px">
                        <a id="search_button" class="btn btn-primary">Search</a>
                    </div>
                
                </div>
            </div>
	
        </div>
	
	
        <div id="resultModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                 
                    <div class="modal-body">
                        <p><b>City: </b><span id="city"></span></p>
                        <p><b>Current Temperature: </b><span id="current_temperature"></span> °C</p>
                        <p><b>Maximum Temperature: </b><span id="max_temperature"></span> °C</p>
                        <p><b>Minimum Temperature: </b><span id="min_temperature"></span> °C</p>
                        <p><b>Wind Speed: </b><span id="wind_speed"></span> m/s</p>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
	
        <script>
            $(document).ready(function(){
                $("#search_button").on("click", function(event){
                    $.ajax({
                        url: 'Requests.php',
                        type: 'POST',
                        datatype: 'json',
                        data: {city_name : $("input[name='search_field'").val()},
                        success: function(data){
                            
                            //parse json
                            parsed = JSON.parse(data);
                            
                            //everything went without errors, display popup with temperatures
                            if(parsed.status == "success"){
                                $("#theAlert").hide();
                                $("#resultModal .modal-title").html("Weather from " + parsed.provider);
                                $('#city').html(parsed.city);
                                $('#current_temperature').html(parsed.current_temp);
                                $('#max_temperature').html(parsed.max_temp);
                                $('#min_temperature').html(parsed.min_temp);
                                $('#wind_speed').html(parsed.wind_speed);
                                $('#resultModal').modal('show');
                            }
                            else{
                                //show bootstrap alert
                                $("#error_message").html(parsed.error_message);
                                $("#theAlert").show();
                                
                            }			
                        }
                    });
                });
            });
        </script>
		
	</body>
</html>